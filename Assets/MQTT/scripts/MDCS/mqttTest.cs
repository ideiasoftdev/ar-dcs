﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System;
using UnityEngine.UI;

public class VirtualComponent {
    public String Label;
    public String Topic;
    public String Value;
    public GameObject PrefabComponente;
}

public class mqttTest : MonoBehaviour {


    public List<VirtualComponent> VirtualComponentList;    
	private MqttClient client;
	private String clientID;
    private String Topic;
    private String Received;
    public Text msgStatus;
    public Text msgTopic;
    private String ipbroker;
    private int port;
    private String user;
    private String password;
    private String TopicPublish;
    private String MessagePublish;

    //UI Controls
    private bool ConectionStatus;
    public Switch btnStatus; 
    public Switch btnVirtual;    

    //Text Markers
    public Text Marker01_Title1;
    public Text Marker01_TAG1;
    public Text Marker01_Title2;
    public Text Marker01_TAG2;
    public Text Marker01_Title3;
    public Text Marker01_TAG3;
    public Text Marker01_Title4;
    public Text Marker01_TAG4;

    public Text Marker02_Title1;
    public Text Marker02_TAG1;
    public Text Marker02_Title2;
    public Text Marker02_TAG2;
    public Text Marker02_Title3;
    public Text Marker02_TAG3;
    public Text Marker02_Title4;
    public Text Marker02_TAG4;

    public Text Marker03_Title1;
    public Text Marker03_TAG1;
    public Text Marker03_Title2;
    public Text Marker03_TAG2;
    public Text Marker03_Title3;
    public Text Marker03_TAG3;
    public Text Marker03_Title4;
    public Text Marker03_TAG4;

    public Text Marker04_Title1;
    public Text Marker04_TAG1;
    public Text Marker04_Title2;
    public Text Marker04_TAG2;
    public Text Marker04_Title3;
    public Text Marker04_TAG3;
    public Text Marker04_Title4;
    public Text Marker04_TAG4;

    public Text Marker05_Title1;
    public Text Marker05_TAG1;
    public Text Marker05_Title2;
    public Text Marker05_TAG2;
    public Text Marker05_Title3;
    public Text Marker05_TAG3;
    public Text Marker05_Title4;
    public Text Marker05_TAG4;

    public Text Marker06_Title1;
    public Text Marker06_TAG1;
    public Text Marker06_Title2;
    public Text Marker06_TAG2;
    public Text Marker06_Title3;
    public Text Marker06_TAG3;
    public Text Marker06_Title4;
    public Text Marker06_TAG4;

    public Text Marker07_Title1;
    public Text Marker07_TAG1;
    public Text Marker07_Title2;
    public Text Marker07_TAG2;
    public Text Marker07_Title3;
    public Text Marker07_TAG3;
    public Text Marker07_Title4;
    public Text Marker07_TAG4;

    public Text Marker08_Title1;
    public Text Marker08_TAG1;
    public Text Marker08_Title2;
    public Text Marker08_TAG2;
    public Text Marker08_Title3;
    public Text Marker08_TAG3;
    public Text Marker08_Title4;
    public Text Marker08_TAG4;

    public Text Marker09_Title1;
    public Text Marker09_TAG1;
    public Text Marker09_Title2;
    public Text Marker09_TAG2;
    public Text Marker09_Title3;
    public Text Marker09_TAG3;
    public Text Marker09_Title4;
    public Text Marker09_TAG4;

    public Text Marker10_Title1;
    public Text Marker10_TAG1;
    public Text Marker10_Title2;
    public Text Marker10_TAG2;
    public Text Marker10_Title3;
    public Text Marker10_TAG3;
    public Text Marker10_Title4;
    public Text Marker10_TAG4;

    public Text Marker11_Title1;
    public Text Marker11_TAG1;
    public Text Marker11_Title2;
    public Text Marker11_TAG2;
    public Text Marker11_Title3;
    public Text Marker11_TAG3;
    public Text Marker11_Title4;
    public Text Marker11_TAG4;

    public Text Marker12_Title1;
    public Text Marker12_TAG1;
    public Text Marker12_Title2;
    public Text Marker12_TAG2;
    public Text Marker12_Title3;
    public Text Marker12_TAG3;
    public Text Marker12_Title4;
    public Text Marker12_TAG4;

    public Text Marker13_Title1;
    public Text Marker13_TAG1;
    public Text Marker13_Title2;
    public Text Marker13_TAG2;
    public Text Marker13_Title3;
    public Text Marker13_TAG3;
    public Text Marker13_Title4;
    public Text Marker13_TAG4;

    public Text Marker14_Title1;
    public Text Marker14_TAG1;
    public Text Marker14_Title2;
    public Text Marker14_TAG2;
    public Text Marker14_Title3;
    public Text Marker14_TAG3;
    public Text Marker14_Title4;
    public Text Marker14_TAG4;

    public Text Marker15_Title1;
    public Text Marker15_TAG1;
    public Text Marker15_Title2;
    public Text Marker15_TAG2;
    public Text Marker15_Title3;
    public Text Marker15_TAG3;
    public Text Marker15_Title4;
    public Text Marker15_TAG4;

    public Text Marker16_Title1;
    public Text Marker16_TAG1;
    public Text Marker16_Title2;
    public Text Marker16_TAG2;
    public Text Marker16_Title3;
    public Text Marker16_TAG3;
    public Text Marker16_Title4;
    public Text Marker16_TAG4;

    public Text Marker17_Title1;
    public Text Marker17_TAG1;
    public Text Marker17_Title2;
    public Text Marker17_TAG2;
    public Text Marker17_Title3;
    public Text Marker17_TAG3;
    public Text Marker17_Title4;
    public Text Marker17_TAG4;

    public Text Marker18_Title1;
    public Text Marker18_TAG1;
    public Text Marker18_Title2;
    public Text Marker18_TAG2;
    public Text Marker18_Title3;
    public Text Marker18_TAG3;
    public Text Marker18_Title4;
    public Text Marker18_TAG4;

    public Text Marker19_Title1;
    public Text Marker19_TAG1;
    public Text Marker19_Title2;
    public Text Marker19_TAG2;
    public Text Marker19_Title3;
    public Text Marker19_TAG3;
    public Text Marker19_Title4;
    public Text Marker19_TAG4;

    public Text Marker20_Title1;
    public Text Marker20_TAG1;
    public Text Marker20_Title2;
    public Text Marker20_TAG2;
    public Text Marker20_Title3;
    public Text Marker20_TAG3;
    public Text Marker20_Title4;
    public Text Marker20_TAG4;



    public bool btnVirtualStatus;

	// Use this for initialization
	void Start () {
		// Zerar a mensagem
        msgStatus.text = "";  
        Topic = "";        

        // Carregar o IP do Servidor
        ipbroker = PlayerPrefs.GetString("broker");   
        port     = PlayerPrefs.GetInt("port");  
        user     = PlayerPrefs.GetString("user"); 
        password = PlayerPrefs.GetString("password"); 
        btnVirtualStatus = false;   

	}



	public void MqttDisconect()
    {
        client.Disconnect();
        //btnStatus.isOn = false;
        msgStatus.text = "Disconnected with the server " + ipbroker;
        ConectionStatus = false;
    }

    public void MqttConnect()
    {
        
        //String ipbroker = "192.168.1.105";

        // Carregar o IP do Servidor
        ipbroker = PlayerPrefs.GetString("broker");   
        port     = PlayerPrefs.GetInt("port");  
        user     = PlayerPrefs.GetString("user"); 
        password = PlayerPrefs.GetString("password");  
        
        client = new MqttClient(IPAddress.Parse(ipbroker), port, false, null);

        // register to message received 
        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

        clientID = Guid.NewGuid().ToString();

        //Conectar
        //this.MqttConnect();
        //btnStatus.isOn = false;

        // subscribe to the topic "/home/temperature" with QoS 2 
        client.Subscribe(new string[] { "m-dcs/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });

        try
        {
            //Connect

            client.Connect(clientID);
            if (client.IsConnected)
            {
                //btnStatus.isOn = true;
                msgStatus.text = "Connected with the server " + ipbroker;
                ConectionStatus = true;
            }
            
        }
        catch(MqttConnectionException err)
        {
            
            //btnStatus.isOn = false;
            msgStatus.text = "Error on connect. Verify the options.";
            Debug.Log(err.Message);
            ConectionStatus = false;
            //msgStatus.text = err.Message;
        }
    }

    public void SwitchMQTT(){
    	if(btnStatus.isOn == true){
    		this.MqttConnect();
            
    	}

    	if(btnStatus.isOn == false){
    		this.MqttDisconect();
            
    	}
    }


    public void SwitchVirtualButton(){ 

    	if(ConectionStatus){
    		if(btnVirtual.isOn == true){
    			client.Publish("bomba1", System.Text.Encoding.UTF8.GetBytes("1"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
    		}

	    	if(btnVirtual.isOn == false){
	    		client.Publish("bomba1", System.Text.Encoding.UTF8.GetBytes("0"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
	    	}
    	}else{
    		msgStatus.text = "Error on send command working offline. Verify the connection.";
    	}

    	
   	
    }

    




	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 

		Topic = e.Topic;
        Received = System.Text.Encoding.UTF8.GetString(e.Message);        
        Debug.Log("Topic:" + Topic + " Received: " + Received ); 
        //this.UpdateMessages();

	} 

    public void UpdateMessages(){
        switch (Topic) {

            #region Marker01
            //Title 1
            case "m-dcs/marker01/title1": {                       
                        Marker01_Title1.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

             //TAG1
            case "m-dcs/marker01/tag1": {                       
                        Marker01_TAG1.text = Received.ToString();                      
                        //Debug.Log(Received);
                        break;
            }

            //Title 2
            case "m-dcs/marker01/title2": {                       
                        Marker01_Title2.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG2
            case "m-dcs/marker01/tag2": {                       
                        Marker01_TAG2.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //Title 3
            case "m-dcs/marker01/title3": {                       
                        Marker01_Title3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG3
            case "m-dcs/marker01/tag3": {                       
                        Marker01_TAG3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //Title 4
            case "m-dcs/marker01/title4": {                       
                        Marker01_Title3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG4
            case "m-dcs/marker01/tag4": {                       
                        Marker01_TAG3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            #endregion 

            #region Marker02
            //Title 1
            case "m-dcs/marker02/title1": {                       
                        Marker02_Title1.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

             //TAG1
            case "m-dcs/marker02/tag1": {                       
                        Marker02_TAG1.text = Received.ToString();                      
                        //Debug.Log(Received);
                        break;
            }

            //Title 2
            case "m-dcs/marker02/title2": {                       
                        Marker02_Title2.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG2
            case "m-dcs/marker02/tag2": {                       
                        Marker02_TAG2.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //Title 3
            case "m-dcs/marker02/title3": {                       
                        Marker02_Title3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG3
            case "m-dcs/marker02/tag3": {                       
                        Marker02_TAG3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //Title 4
            case "m-dcs/marker02/title4": {                       
                        Marker02_Title4.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG4
            case "m-dcs/marker02/tag4": {                       
                        Marker02_TAG4.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            #endregion

            #region Marker03
            //Title 1
            case "m-dcs/marker03/title1": {                       
                        Marker03_Title1.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

             //TAG1
            case "m-dcs/marker03/tag1": {                       
                        Marker03_TAG1.text = Received.ToString();                      
                        //Debug.Log(Received);
                        break;
            }

            //Title 2
            case "m-dcs/marker03/title2": {                       
                        Marker03_Title2.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG2
            case "m-dcs/marker03/tag2": {                       
                        Marker03_TAG2.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //Title 3
            case "m-dcs/marker03/title3": {                       
                        Marker03_Title3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG3
            case "m-dcs/marker03/tag3": {                       
                        Marker03_TAG3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //Title 4
            case "m-dcs/marker03/title4": {                       
                        Marker03_Title3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            //TAG4
            case "m-dcs/marker03/tag4": {                       
                        Marker03_TAG3.text = Received.ToString();                        
                        //Debug.Log(Received);
                        break;
            }

            #endregion

            #region Marker04
            //Title 1
            case "m-dcs/marker04/title1": {                       
                Marker04_Title1.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG1
            case "m-dcs/marker04/tag1": {                       
                Marker04_TAG1.text = Received.ToString();                      
                //Debug.Log(Received);
                break;
            }

            //Title 2
        case "m-dcs/marker04/title2": {                       
                Marker04_Title2.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG2
        case "m-dcs/marker04/tag2": {                       
                Marker04_TAG2.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //Title 3
        case "m-dcs/marker04/title3": {                       
                Marker04_Title3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG3
        case "m-dcs/marker04/tag3": {                       
                Marker04_TAG3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //Title 4
        case "m-dcs/marker04/title4": {                       
                Marker04_Title3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG4
        case "m-dcs/marker04/tag4": {                       
                Marker04_TAG3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            #endregion

            #region Marker05
            //Title 1
            case "m-dcs/marker05/title1": {                       
                Marker05_Title1.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG1
        case "m-dcs/marker05/tag1": {                       
                Marker05_TAG1.text = Received.ToString();                      
                //Debug.Log(Received);
                break;
            }

            //Title 2
        case "m-dcs/marker05/title2": {                       
                Marker05_Title2.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG2
        case "m-dcs/marker05/tag2": {                       
                Marker05_TAG2.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //Title 3
        case "m-dcs/marker05/title3": {                       
                Marker05_Title3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG3
        case "m-dcs/marker05/tag3": {                       
                Marker05_TAG3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //Title 4
        case "m-dcs/marker05/title4": {                       
                Marker05_Title4.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG4
        case "m-dcs/marker05/tag4": {                       
                Marker05_TAG4.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            #endregion

            #region Marker06
            //Title 1
            case "m-dcs/marker06/title1": {                       
                Marker06_Title1.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG1
        case "m-dcs/marker06/tag1": {                       
                Marker06_TAG1.text = Received.ToString();                      
                //Debug.Log(Received);
                break;
            }

            //Title 2
        case "m-dcs/marker06/title2": {                       
                Marker06_Title2.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG2
        case "m-dcs/marker06/tag2": {                       
                Marker06_TAG2.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //Title 3
        case "m-dcs/marker06/title3": {                       
                Marker06_Title3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG3
        case "m-dcs/marker06/tag3": {                       
                Marker06_TAG3.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //Title 4
        case "m-dcs/marker06/title4": {                       
                Marker06_Title4.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            //TAG4
        case "m-dcs/marker06/tag4": {                       
                Marker06_TAG4.text = Received.ToString();                        
                //Debug.Log(Received);
                break;
            }

            #endregion

            #region Marker07
            //Title 1
            case "m-dcs/marker07/title1":
                {
                    Marker07_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker07/tag1":
                {
                    Marker07_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker07/title2":
                {
                    Marker07_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker07/tag2":
                {
                    Marker07_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker07/title3":
                {
                    Marker07_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker07/tag3":
                {
                    Marker07_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker07/title4":
                {
                    Marker07_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker07/tag4":
                {
                    Marker07_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker08
            //Title 1
            case "m-dcs/marker08/title1":
                {
                    Marker08_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker08/tag1":
                {
                    Marker08_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker08/title2":
                {
                    Marker08_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker08/tag2":
                {
                    Marker08_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker08/title3":
                {
                    Marker08_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker08/tag3":
                {
                    Marker08_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker08/title4":
                {
                    Marker08_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker08/tag4":
                {
                    Marker08_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker09
            //Title 1
            case "m-dcs/marker09/title1":
                {
                    Marker09_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker09/tag1":
                {
                    Marker09_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker09/title2":
                {
                    Marker09_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker09/tag2":
                {
                    Marker09_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker09/title3":
                {
                    Marker09_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker09/tag3":
                {
                    Marker09_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker09/title4":
                {
                    Marker09_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker09/tag4":
                {
                    Marker09_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker10
            //Title 1
            case "m-dcs/marker10/title1":
                {
                    Marker10_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker10/tag1":
                {
                    Marker10_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker10/title2":
                {
                    Marker10_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker10/tag2":
                {
                    Marker10_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker10/title3":
                {
                    Marker10_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker10/tag3":
                {
                    Marker10_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker10/title4":
                {
                    Marker10_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker10/tag4":
                {
                    Marker10_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker11
            //Title 1
            case "m-dcs/marker11/title1":
                {
                    Marker11_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker11/tag1":
                {
                    Marker11_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker11/title2":
                {
                    Marker11_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker11/tag2":
                {
                    Marker11_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker11/title3":
                {
                    Marker11_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker11/tag3":
                {
                    Marker11_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker11/title4":
                {
                    Marker11_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker11/tag4":
                {
                    Marker11_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker12
            //Title 1
            case "m-dcs/marker12/title1":
                {
                    Marker12_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker12/tag1":
                {
                    Marker12_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker12/title2":
                {
                    Marker12_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker12/tag2":
                {
                    Marker12_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker12/title3":
                {
                    Marker12_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker12/tag3":
                {
                    Marker12_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker12/title4":
                {
                    Marker12_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker12/tag4":
                {
                    Marker12_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker13
            //Title 1
            case "m-dcs/marker13/title1":
                {
                    Marker13_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker13/tag1":
                {
                    Marker13_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker13/title2":
                {
                    Marker13_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker13/tag2":
                {
                    Marker13_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker13/title3":
                {
                    Marker13_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker13/tag3":
                {
                    Marker13_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker13/title4":
                {
                    Marker13_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker13/tag4":
                {
                    Marker13_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker14
            //Title 1
            case "m-dcs/marker14/title1":
                {
                    Marker14_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker14/tag1":
                {
                    Marker14_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker14/title2":
                {
                    Marker14_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker14/tag2":
                {
                    Marker14_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker14/title3":
                {
                    Marker14_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker14/tag3":
                {
                    Marker14_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker14/title4":
                {
                    Marker14_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker14/tag4":
                {
                    Marker14_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker15
            //Title 1
            case "m-dcs/marker15/title1":
                {
                    Marker15_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker15/tag1":
                {
                    Marker15_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker15/title2":
                {
                    Marker15_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker15/tag2":
                {
                    Marker15_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker15/title3":
                {
                    Marker15_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker15/tag3":
                {
                    Marker15_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker15/title4":
                {
                    Marker15_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker15/tag4":
                {
                    Marker15_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker16
            //Title 1
            case "m-dcs/marker16/title1":
                {
                    Marker16_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker16/tag1":
                {
                    Marker16_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker16/title2":
                {
                    Marker16_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker16/tag2":
                {
                    Marker16_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker16/title3":
                {
                    Marker16_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker16/tag3":
                {
                    Marker16_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker16/title4":
                {
                    Marker16_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker16/tag4":
                {
                    Marker16_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker17
            //Title 1
            case "m-dcs/marker17/title1":
                {
                    Marker17_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker17/tag1":
                {
                    Marker17_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker17/title2":
                {
                    Marker17_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker17/tag2":
                {
                    Marker17_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker17/title3":
                {
                    Marker17_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker17/tag3":
                {
                    Marker17_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker17/title4":
                {
                    Marker17_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker17/tag4":
                {
                    Marker17_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker18
            //Title 1
            case "m-dcs/marker18/title1":
                {
                    Marker18_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker18/tag1":
                {
                    Marker18_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker18/title2":
                {
                    Marker18_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker18/tag2":
                {
                    Marker18_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker18/title3":
                {
                    Marker18_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker18/tag3":
                {
                    Marker18_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker18/title4":
                {
                    Marker18_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker18/tag4":
                {
                    Marker18_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker19
            //Title 1
            case "m-dcs/marker19/title1":
                {
                    Marker19_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker19/tag1":
                {
                    Marker19_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker19/title2":
                {
                    Marker19_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker19/tag2":
                {
                    Marker19_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker19/title3":
                {
                    Marker19_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker19/tag3":
                {
                    Marker19_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker19/title4":
                {
                    Marker19_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker19/tag4":
                {
                    Marker19_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            #endregion

            #region Marker20
            //Title 1
            case "m-dcs/marker20/title1":
                {
                    Marker20_Title1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG1
            case "m-dcs/marker20/tag1":
                {
                    Marker20_TAG1.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 2
            case "m-dcs/marker20/title2":
                {
                    Marker20_Title2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG2
            case "m-dcs/marker20/tag2":
                {
                    Marker20_TAG2.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 3
            case "m-dcs/marker20/title3":
                {
                    Marker20_Title3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG3
            case "m-dcs/marker20/tag3":
                {
                    Marker20_TAG3.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //Title 4
            case "m-dcs/marker20/title4":
                {
                    Marker20_Title4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

            //TAG4
            case "m-dcs/marker20/tag4":
                {
                    Marker20_TAG4.text = Received.ToString();
                    //Debug.Log(Received);
                    break;
                }

                #endregion
            
        }
    }

	

	// Update is called once per frame
	void Update () {
      
        this.UpdateMessages();

    }
}
