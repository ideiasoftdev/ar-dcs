﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System;
using UnityEngine.UI;

public class mqttDemo1: MonoBehaviour {

    private MqttClient client;
	private String clientID;
    private String Topic;
    private String Received;
    public Text msgStatus;
    public Text msgTopic;
    private String ipbroker;
    private int port;
    private String user;
    private String password;
    private String TopicPublish;
    private String MessagePublish;

    //UI Controls
    private bool ConectionStatus;
    public Switch btnStatus; 
    public Switch btnVirtual;    

    //Text Markers
    public Text Marker01_Title;
    public Text Marker01_TAG;    

    public Text Marker02_Title;
    public Text Marker02_TAG;    

    public Text Marker03_Title;
    public Text Marker03_TAG;    

    public Text Marker04_Title1;
    public Text Marker04_TAG1;

    public Text test_marker_lbl;
    public Text test_marker_val;
    private Dictionary<String, String> dict;
    private Dictionary<String, Text> texts_dict;

    public bool btnVirtualStatus;

	// Use this for initialization
	void Start () {
		// Zerar a mensagem
        msgStatus.text = "";  
        Topic = "";        

        // Carregar o IP do Servidor
        ipbroker = PlayerPrefs.GetString("broker");   
        port     = PlayerPrefs.GetInt("port");  
        user     = PlayerPrefs.GetString("user"); 
        password = PlayerPrefs.GetString("password"); 
        btnVirtualStatus = false;

        dict = new Dictionary<String, String>();
        texts_dict = new Dictionary<String, Text>();
        texts_dict.Add("m-dcs/marker01/title1", Marker01_Title);
        texts_dict.Add("test_topic", test_marker_val);

	}

	public void MqttDisconect(){
        client.Disconnect();
        //btnStatus.isOn = false;
        msgStatus.text = "Disconnected with the server " + ipbroker;
        ConectionStatus = false;
    }

    public void MqttConnect(){
        
        //String ipbroker = "192.168.1.105";

        // Carregar o IP do Servidor
        ipbroker = PlayerPrefs.GetString("broker");   
        port     = PlayerPrefs.GetInt("port");  
        user     = PlayerPrefs.GetString("user"); 
        password = PlayerPrefs.GetString("password");  
        
        client = new MqttClient(IPAddress.Parse(ipbroker), port, false, null);

        // register to message received 
        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

        clientID = Guid.NewGuid().ToString();

        //Conectar
        //this.MqttConnect();
        //btnStatus.isOn = false;

        // subscribe to the topic "/home/temperature" with QoS 2 
        client.Subscribe(new string[] { "m-dcs/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
        client.Subscribe(new string[] { "test_topic" }, new byte[] {MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE});

        try
        {
            //Connect

            client.Connect(clientID);
            if (client.IsConnected)
            {
                //btnStatus.isOn = true;
                msgStatus.text = "Connected with the server " + ipbroker;
                ConectionStatus = true;
            }
            
        }
        catch(MqttConnectionException err)
        {
            
            //btnStatus.isOn = false;
            msgStatus.text = "Error on connect. Verify the options.";
            Debug.Log(err.Message);
            ConectionStatus = false;
            msgStatus.text = err.Message;
        }
    }

    public void SwitchMQTT(){
    	if(btnStatus.isOn == true){
    		this.MqttConnect();
            
    	}

    	if(btnStatus.isOn == false){
    		this.MqttDisconect();
            
    	}
    }

    public void SwitchVirtualButton(){ 

    	if(ConectionStatus){
    		if(btnVirtual.isOn == true){
    			client.Publish("bomba1", System.Text.Encoding.UTF8.GetBytes("1"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
    		}

	    	if(btnVirtual.isOn == false){
	    		client.Publish("bomba1", System.Text.Encoding.UTF8.GetBytes("0"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
	    	}
    	}else{
    		msgStatus.text = "Error on send command working offline. Verify the connection.";
    	}   	
   	
    }
    
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
		Topic = e.Topic;
        Debug.Log("Topic ------ "+Topic);
        Received = System.Text.Encoding.UTF8.GetString(e.Message);        
        Debug.Log("Topic:" + Topic + " Received: " + Received );

        dict.Add(Topic, Received);
	} 

    public void UpdateMessages(){
         
    }

	// Update is called once per frame
	void Update () {
        foreach (KeyValuePair<String, String> pair in dict)
        {
            Debug.Log("Key: " + pair.Key.ToString() + " - Value: " + pair.Key.ToString());
            texts_dict[pair.Key.ToString()].text = pair.Value.ToString();
            dict.Remove(pair.Key.ToString());
        }
    }
}
