﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using UnityEngine.UI;

using System;

public class MqttMessage{
	public string topic { get; set; }
	public String message { get; set; }
}

public class mqttSimple : MonoBehaviour {
	private String clientID;
	public String topic;
	public String msg;
	public String ipbroker;
	public int port;
	public String user;
	public String password;
	//UI Controls
    private bool ConectionStatus;
    public Switch btnStatus; 
	public Text msgStatus;

    public Text display1_label;
    public Text display1_value;
    public Text display2_label;
    public Text display2_value;
    public Text display3_label;
    public Text display3_value;
    public Text display4_label;
    public Text display4_value;

	public List<MqttMessage> messages_list;
	public List<MqttMessage> messages_list_copy;
	public Dictionary<string, Text> components_dict;

	private MqttClient client;
	// Use this for initialization
	void Start () {

		messages_list = new List<MqttMessage>();
		messages_list_copy = new List<MqttMessage>();
		components_dict = new Dictionary<string, Text>();

        //Registro dos topicos mqtt e componentes
        components_dict.Add("m-dcs/marker05/title1", display1_label);
        components_dict.Add("m-dcs/marker05/tag1", display1_value);
        components_dict.Add("m-dcs/marker08/title1", display2_label);
        components_dict.Add("m-dcs/marker08/tag1", display2_value);
        components_dict.Add("m-dcs/marker02/title1", display3_label);
        components_dict.Add("m-dcs/marker02/tag1", display3_value);
        components_dict.Add("m-dcs/marker07/title1", display4_label);
        components_dict.Add("m-dcs/marker07/tag1", display4_value);

	}


	public void MqttConnect(){
        
        //String ipbroker = "192.168.1.105";

        // Carregar o IP do Servidor
        ipbroker = PlayerPrefs.GetString("broker");   
        port     = PlayerPrefs.GetInt("port");  
        var user     = PlayerPrefs.GetString("user"); 
        var password = PlayerPrefs.GetString("password");  
        
        client = new MqttClient(IPAddress.Parse(ipbroker), port, false, null);

        // register to message received 
        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

        clientID = Guid.NewGuid().ToString();

        //Conectar
        //this.MqttConnect();
        //btnStatus.isOn = false;

        // subscribe to the topic "/home/temperature" with QoS 2 
        client.Subscribe(new string[] { "m-dcs/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
        client.Subscribe(new string[] { "test_topic" }, new byte[] {MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE});

        try
        {
            //Connect

            client.Connect(clientID);
            if (client.IsConnected)
            {
                //btnStatus.isOn = true;
                msgStatus.text = "Connected with the server " + ipbroker;
                ConectionStatus = true;
            }
            
        }
        catch(MqttConnectionException err)
        {
            
            //btnStatus.isOn = false;
            msgStatus.text = "Error on connect. Verify the options.";
            Debug.Log(err.Message);
            ConectionStatus = false;
            msgStatus.text = err.Message;
        }
    }

	public void MqttDisconect(){
        client.Disconnect();
        //btnStatus.isOn = false;
        msgStatus.text = "Disconnected with the server " + ipbroker;
        ConectionStatus = false;
    }

    public void SwitchMQTT(){
    	if(btnStatus.isOn == true){
    		this.MqttConnect();
            
    	}

    	if(btnStatus.isOn == false){
    		this.MqttDisconect();
            
    	}
    }



	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
		msg = System.Text.Encoding.UTF8.GetString(e.Message);
		topic = e.Topic.ToString();
		Debug.Log("Received: " + System.Text.Encoding.UTF8.GetString(e.Message)  );
		
		messages_list.Add(new MqttMessage() {topic=topic, message=msg});
	} 

	void OnGUI(){
        /*
		if ( GUI.Button (new Rect (20,40,80,20), "Level 1")) {
			Debug.Log("sending...");
			client.Publish("test_topic", System.Text.Encoding.UTF8.GetBytes("Sending from Unity3D!!!"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
			Debug.Log("sent");
		}

        */
	}

	// Update is called once per frame
	void Update () {
		messages_list_copy = messages_list;
		messages_list_copy.ForEach(delegate(MqttMessage m){
			if(components_dict.ContainsKey(m.topic)){
				components_dict[m.topic].text = m.message;
			}
			messages_list.Remove(m);
		});
		messages_list_copy.Clear();
	}
}
