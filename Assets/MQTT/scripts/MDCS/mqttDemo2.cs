﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using UnityEngine.UI;

using System;

public class mqttDemo2 : MonoBehaviour {
	public String topic;
	public String msg;

    public Text display1_label;
    public Text display1_value;
    public Text display2_label;
    public Text display2_value;
    public Text display3_label;
    public Text display3_value;
    public Text display4_label;
    public Text display4_value;
    public Text display5_label;
    public Text display5_value;
    public Text display6_label;
    public Text display6_value;
    public Text display7_label;
    public Text display7_value;
    public Text display8_label;
    public Text display8_value;
    public Text display9_label;
    public Text display9_value;
    public Text display10_label;
    public Text display10_value;

	public List<MqttMessage> messages_list;
	public List<MqttMessage> messages_list_copy;
	public Dictionary<string, Text> components_dict;

	private MqttClient client;
	// Use this for initialization
	void Start () {

		messages_list = new List<MqttMessage>();
		messages_list_copy = new List<MqttMessage>();
		components_dict = new Dictionary<string, Text>();

        //Registro dos topicos mqtt e componentes
        components_dict.Add("m-dcs/marker02/title1", display2_label);
        components_dict.Add("m-dcs/marker02/tag1", display2_value);
        components_dict.Add("m-dcs/marker03/title1", display3_label);
        components_dict.Add("m-dcs/marker03/tag1", display3_value);
        components_dict.Add("m-dcs/marker04/title1", display4_label);
        components_dict.Add("m-dcs/marker04/tag1", display4_value);
        components_dict.Add("m-dcs/marker05/title1", display5_label);
        components_dict.Add("m-dcs/marker05/tag1", display5_value);
        components_dict.Add("m-dcs/marker06/title1", display6_label);
        components_dict.Add("m-dcs/marker06/tag1", display6_value);
        components_dict.Add("m-dcs/marker07/title1", display7_label);
        components_dict.Add("m-dcs/marker07/tag1", display7_value);
        components_dict.Add("m-dcs/marker08/title1", display8_label);
        components_dict.Add("m-dcs/marker08/tag1", display8_value);

		// create client instance 
		string ipbroker = PlayerPrefs.GetString("broker");   
        int port     = PlayerPrefs.GetInt("port"); 
		client = new MqttClient(IPAddress.Parse(ipbroker),port , false , null ); 
		
		// register to message received 
		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 
		
		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId); 
		
		// subscribe to the topic "/home/temperature" with QoS 2 
		client.Subscribe(new string[] { "m-dcs/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }); 

	}
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
		msg = System.Text.Encoding.UTF8.GetString(e.Message);
		topic = e.Topic.ToString();
		Debug.Log("Received: " + System.Text.Encoding.UTF8.GetString(e.Message)  );
		
		messages_list.Add(new MqttMessage() {topic=topic, message=msg});
	} 

	void OnGUI(){
        /*
		if ( GUI.Button (new Rect (20,40,80,20), "Level 1")) {
			Debug.Log("sending...");
			client.Publish("test_topic", System.Text.Encoding.UTF8.GetBytes("Sending from Unity3D!!!"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
			Debug.Log("sent");
		}

        */
	}

	// Update is called once per frame
	void Update () {
		messages_list_copy = messages_list;
		messages_list_copy.ForEach(delegate(MqttMessage m){
			if(components_dict.ContainsKey(m.topic)){
				components_dict[m.topic].text = m.message;
			}
			messages_list.Remove(m);
		});
		messages_list_copy.Clear();
	}
}
