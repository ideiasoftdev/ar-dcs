﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Editor : MonoBehaviour {

	public GameObject posicionador;
	public GameObject marcadorBase;
	public GameObject prefabComponente;
	public Text Position;	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		posicionador = GameObject.FindGameObjectWithTag("point") as GameObject;
		float point_position_x = posicionador.transform.position.x;
		float point_position_y = posicionador.transform.position.y;
		float point_position_z = posicionador.transform.position.z;
		Debug.Log("Point: (" + point_position_x + "," + point_position_y + "," + point_position_z + ")");
		Position.text = "Point: (" + point_position_x + "," + point_position_y + "," + point_position_z + ")";
	}


	public void SetPosition(){
		
		//myComponent.transform.position = posicionador.transform.position;
		//marcadorBase.FindGameObjectWithTag("base");
		//marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
		//(Instantiate(prefabComponente) as GameObject).transform.parent = marcadorBase.transform;
		//marcadorBase.transform.parent = myComponent;
		GameObject myComponent = (GameObject)Instantiate(prefabComponente,new Vector3(0,10,0),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;
		
	}


}
