﻿using ProgressBar;
using System.Collections;
using UnityEngine;

public class exampleProgressRadial : MonoBehaviour
{
    ProgressRadialBehaviour BarBehaviour;
    float UpdateDelay = 2f;

    IEnumerator Start ()
    {
        BarBehaviour = GetComponent<ProgressRadialBehaviour>();
        while (true)
        {
            yield return new WaitForSeconds(UpdateDelay);
            BarBehaviour.Value = Random.value * 100;
            print("new value: " + BarBehaviour.Value);
        }
    }
}
