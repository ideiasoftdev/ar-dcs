using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EditorLocalizacao : MonoBehaviour {

	private GameObject posicionador;
	private GameObject marcadorBase;
	public GameObject prefab_WidgetArrow1;
	public GameObject prefab_WidgetArrow2;
	public GameObject prefab_WidgetArrow3;
	public GameObject prefab_WidgetDisplay1;
	public GameObject prefab_WidgetGauge1;
	public GameObject prefab_WidgetButtons1;
	public GameObject prefab_WidgetProgress1;
	public GameObject prefab_WidgetProgress2;
	public GameObject prefab_WidgetSwitch1;	
	public GameObject prefab_WidgetLineChart;
	public float Value;
	
	
	public Text Position;	
	private String ActivePosition;
	private float ActiveX;
	private float ActiveY;
	private float ActiveZ;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

		posicionador = GameObject.FindGameObjectWithTag("point") as GameObject;
		ActiveX = posicionador.transform.position.x;
		ActiveY = posicionador.transform.position.y;
		ActiveZ = posicionador.transform.position.z;
		ActivePosition = "Point: (" + ActiveX + "," + ActiveY + "," + ActiveZ + ")";
		//Debug.Log(ActivePosition);
		Position.text = ActivePosition;
	
	}

	public void FixWidgetArrow1(){
		//GameObject myComponent = (GameObject)Instantiate(prefabComponente,new Vector3(0,10,0),transform.rotation);
        //myComponent.transform.parent = marcadorBase.transform;
        Debug.Log("Fixar posicao em: " + ActivePosition);

        //Inserir um cubo na posição
        //var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //cube.transform.position = new Vector3(ActiveX, ActiveY, ActiveZ);

        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetArrow1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;
        

	}

	public void FixWidgetArrow2(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetArrow2,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetArrow3(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetArrow3,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetDisplay1(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetDisplay1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetGauge1(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetGauge1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetButtons1(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetButtons1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetProgress1(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetProgress1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetProgress2(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetProgress1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetSwitch1(){		
        Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetSwitch1,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        

	}

	public void FixWidgetLineChart(){
		Debug.Log("Fixar posicao em: " + ActivePosition);   
        //Fixando pontos na base
        marcadorBase = GameObject.FindGameObjectWithTag("base") as GameObject;
        GameObject myComponent = (GameObject)Instantiate(prefab_WidgetLineChart,new Vector3(ActiveX,ActiveY,ActiveZ),transform.rotation);
        myComponent.transform.parent = marcadorBase.transform;        
	}




}
