﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Diversos : MonoBehaviour {

	public Renderer rend;
	float timer = 1.0f;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
		StartCoroutine(BlinkCanvas());

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseEnter(){
		rend.material.color = Color.blue;
	}

	void OnMouseOver() {
		//rend.material.color = Color.green;
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("Clicou");
		}


	}

	void OnMouseExit() {
		rend.material.color = Color.red;
	}

	IEnumerator BlinkCanvas(){
		Image img =  GameObject.Find("MyPanel").GetComponent<Image>();
		while (true) {
			yield return new WaitForSeconds(1.5f);
			img.color = UnityEngine.Color.red;
			yield return new WaitForSeconds(1.5f);
			img.color = UnityEngine.Color.cyan;
		}

	}




}
