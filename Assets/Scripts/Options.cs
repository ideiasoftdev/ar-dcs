﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // Required when Using UI elements.

public class Options : MonoBehaviour 
{
	public InputField serverInputField;
	public InputField portInputField;
	public InputField userInputField;
	public InputField passwordInputField;

	bool ShowOptions = false;
	bool ShowComponents = false;

	public Canvas CanvasComponents;
	public Canvas CanvasOptions;
	
	public void Start()
	{
		this.LoadPreferences();
	}

	public void Update(){
		
		if(ShowOptions == false){		
			//Esconde os itens
			CanvasOptions.enabled = false; 
		}

		if(ShowOptions == true){
			//Mostra os itens
			CanvasOptions.enabled = true; 
		}

		if(ShowComponents == false){
			//CanvasComponents.enabled = false;
		}

		if(ShowOptions == true){
			//CanvasComponents.enabled = true;
		}

	}

	public void ToggleOptions(){
		if(ShowOptions == false){
			ShowOptions = true;
		}else{
			ShowOptions = false;
		}
	}

	public void ToggleComponents(){
		if(ShowComponents == false){
			ShowComponents = true;
		}else{
			ShowComponents = false;
		}
	}

	public void Exit(){
		Application.Quit();
	}


	public void SavePreferences(){
		PlayerPrefs.SetString("broker", serverInputField.text);
		int port = int.Parse(portInputField.text);    
		PlayerPrefs.SetInt("port", port);    
		PlayerPrefs.SetString("user", "qqrum"); 
		PlayerPrefs.SetString("password", "ahoy"); 
		//this.ToggleOptions();      
	}

	public void LoadPreferences(){
		serverInputField.text   = PlayerPrefs.GetString("broker");    
		string port = (PlayerPrefs.GetInt("port")).ToString();
		portInputField.text     = port;    
		//userInputField.text     = PlayerPrefs.GetString("user"); 
		//passwordInputField.text = PlayerPrefs.GetString("password");       
	}

	public void LoadDemo1(){
		Application.LoadLevel("smar");
	}

	public void LoadDemo1Cardboard(){
		Application.LoadLevel("demo1_oculos");
	}

	public void LoadDemo2(){
		Application.LoadLevel("demo2");
	}

	public void LoadDemo2Cardboard(){
		Application.LoadLevel("demo2_oculos");
	}


	public void LoadDemo3(){
		Application.LoadLevel("demo3");
	}

	public void LoadDemo4(){
		
	}



}